package pl.codementors.FXMLNotepad;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Locale;
import java.util.ResourceBundle;


public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
//        Locale currentLocale = new Locale("pl");
        Parent root = FXMLLoader.load(getClass().getResource("/pl.codementors.notepad.view/Notepad.fxml"),
                ResourceBundle.getBundle("pl.codementors.notepad.pl.codementors.notepad.view.messages.notepad_msg"));

        Scene scene = new Scene(root, 600, 600);
        stage.setScene(scene);
        stage.show();
    }
}
