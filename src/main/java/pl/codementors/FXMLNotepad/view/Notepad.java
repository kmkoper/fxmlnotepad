
package pl.codementors.FXMLNotepad.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Notepad implements Initializable {
    private static final Logger log = Logger.getLogger(Notepad.class.getCanonicalName());

    @FXML
    private Button save;

    @FXML
    private Button open;

    @FXML
    private TextArea textArea;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @FXML
    private void saveFile(ActionEvent actionEvent) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(null);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(textArea.getText());
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    @FXML
    private void openFile(ActionEvent actionEvent) {
        String wholeText = "";
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(null);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            while (br.ready()) {
                String text = br.readLine();
                wholeText += text + "\n";
            }
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        textArea.setText(wholeText);
    }

}
